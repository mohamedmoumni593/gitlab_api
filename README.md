# GITLAB API

python-gitlab provides a convenient way to interact with gitlab api :

## getting read-only access for public ressources by creating a GitLab instance
```python
gl = gitlab.Gitlab()
```

## you can also get access for private ressources by giving private access token
```python
gl = gitlab.Gitlab("https:gitlab.example.com", private_token="blablablabla")
```

## getting all the available projects for the current user provided with access token

```python
own_projects = gl.projects.list(get_all=True, owned=True)
```

## get project by id

```python
project = gl.projects.get(ID)
```

## create a project
```python
project = gl.projects.create({'name':'my new project'})
```

## create a project in a group
```python
group_id = gl.groups.list(search="my-group")[0].id
project = gl.projects.create({'name':'myproject', 'namespace_id':group_id})
```

## list project's groups

```python
groups = project.groups.list()
```

## delete a project
```python
gl.projects.delete(project_id)
```

## get the branches of a specific repository
```python
branches = project.branches.list()
```

## Get a single repository branch
```python
branch = project.branches.get('main')
```

## Create a repository branch:
```python
branch = project.branches.create({'branch': 'feature1','ref': 'main'})
```

## Delete a repository branch
```python
project.branches.delete('feature1')
```

## list the commits of a project
```python
commits = project.commits.list()
```

## create a commit
```python
data={
    'branch':'main',
    'commit_message':'blah blah blah',
    'actions':[
        {
            'action': 'create',
            'file_path': 'README.rst',
            'content': open('path/to/file.rst').read(),
        },
        {
            # Binary files need to be base64 encoded
            'action': 'create',
            'file_path': 'logo.png',
            'content': base64.b64encode(open('logo.png', mode='r+b').read()).decode(),
            'encoding': 'base64',
        }
    ]
}
commit = project.commits.create(data)
```

## get commit detail
```python
commit = project.commits.get(commit_id)
```

## Get the diff for a commit
```python
diff = commit.diff()
```

## List the merge requests related to a commit
```python
commit.merge_requests()
```

## Get the comments for a commit
```python
comments = commit.comments.list()
```

## Add a comment on a commit
```python
commit = commit.comments.create({'note': 'This is a nice comment'})
```

## List the statuses for a commit
```python
statues = commit.statues.list()
```

## Change the status of a commit:
```python
commit.statuses.create({'state': 'success'})
```

## Get Project's Pipeline
```python
project.pipeline.list()
```

## test
